class TopicObject():
    def __init__(self, topic, header):
        self.topic = topic
        self.header = header
        self.content = ''

    def get_topic(self):
        return self.topic

    def get_header(self):
        return self.header

    def get_content(self):
        return self.content

    def set_topic(self, topic):
        self.topic = topic

    def set_header(self, header):
        self.header = header

    def set_content(self, content):
        self.content = content
