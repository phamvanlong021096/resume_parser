import os
from flask import Flask, request, render_template, jsonify
from cv_parser import get_topic, get_topic_header, read_resume_docx, find_content, get_topic_object, \
    get_list_skill, find_name, find_address, find_birthday, find_email, find_phone, find_gender, find_university, \
    find_degree, find_skill, find_position, find_exp, find_skill_language
from werkzeug.utils import secure_filename
from topic_object import TopicObject

UPLOAD_FOLDER = os.path.dirname(__file__)
ALLOWED_EXTENSIONS = ['docx']

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
topics = get_topic('topic.txt')
headers = get_topic_header(topics)
list_skill = get_list_skill('rule_skill.txt')


@app.route("/todo/api/v1.0/extract_resume", methods=['POST'])
def response_extract_resume_api():
    resume = request.form.get('resume')
    resume = 'thông tin cá nhân\n' + resume.lower() + '\nunknown\n'

    # create topic objects
    topic_objs = [TopicObject(t, headers[t]) for t in topics]

    # extract topic
    for tp_obj1 in topic_objs:
        for tp_obj2 in topic_objs:
            if tp_obj1.get_topic() != tp_obj2.get_topic():
                check, content = find_content(resume, tp_obj1, tp_obj2, topic_objs)
                if check:
                    tp_obj1.set_content(content)

    # get named entity by rule
    info_topic_obj = get_topic_object('thong_tin_ca_nhan', topic_objs)
    info_text = info_topic_obj.get_content()

    name_entity_rule = {'name': find_name(info_text), 'address': find_address(info_text),
                        'birthday': find_birthday(info_text), 'email': find_email(info_text),
                        'phone': find_phone(info_text), 'gender': find_gender(info_text),
                        'university': find_university(resume), 'degree': find_degree(resume),
                        'skill': find_skill(resume, list_skill), 'position': find_position(resume),
                        'exp': find_exp(resume), 'skill_language': find_skill_language(resume)}

    return jsonify({'result': True, 'name_entity': name_entity_rule})


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route("/todo/api/v1.0/extract_file", methods=['POST'])
def response_extract_file_api():
    file = request.files['file']
    if file:
        filename = secure_filename(file.filename)
        path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        # print(path)
        file.save(path)
        # read file
        resume_response = read_resume_docx(path)
        resume = 'thông tin cá nhân\n' + resume_response.lower() + '\nunknown\n'

        os.remove(path)

        # create topic objects
        topic_objs = [TopicObject(t, headers[t]) for t in topics]

        # extract topic
        for tp_obj1 in topic_objs:
            for tp_obj2 in topic_objs:
                if tp_obj1.get_topic() != tp_obj2.get_topic():
                    check, content = find_content(resume, tp_obj1, tp_obj2, topic_objs)
                    if check:
                        tp_obj1.set_content(content)

        # get named entity by rule
        info_topic_obj = get_topic_object('thong_tin_ca_nhan', topic_objs)
        info_text = info_topic_obj.get_content()

        name_entity_rule = {'name': find_name(info_text), 'address': find_address(info_text),
                            'birthday': find_birthday(info_text), 'email': find_email(info_text),
                            'phone': find_phone(info_text), 'gender': find_gender(info_text),
                            'university': find_university(resume), 'degree': find_degree(resume),
                            'skill': find_skill(resume, list_skill), 'position': find_position(resume),
                            'exp': find_exp(resume), 'skill_language': find_skill_language(resume)}

        return jsonify({'result': True, 'name_entity': name_entity_rule, 'text': resume_response})
    else:
        return jsonify({'result': False})


@app.route("/", methods=['GET'])
def test():
    return jsonify({'test': 'Test ok'})


if __name__ == '__main__':
    app.run(debug=True)
